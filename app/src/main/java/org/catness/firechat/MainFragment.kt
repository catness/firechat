package org.catness.firechat

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import org.catness.firechat.databinding.FragmentMainBinding
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class MainFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    companion object {
        const val SIGN_IN_REQUEST_CODE = 1001
        const val MESSAGES_CHILD = "messages";
        const val LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif"
    }

    private val viewModel by viewModels<LoginViewModel>()
    private lateinit var binding: FragmentMainBinding
    private lateinit var adapter: FireChatAdapter
    private var optionsMenu: Menu? = null

    private var username: String? = null
    private var photourl = ""
    private var uid = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)

        val application = requireNotNull(this.activity).application
        binding.setLifecycleOwner(this)

        val layoutManager = LinearLayoutManager(application)
        layoutManager.stackFromEnd = true
        binding.messageList.layoutManager = layoutManager

        var query = FirebaseDatabase.getInstance()
            .reference
            .child(MESSAGES_CHILD)
            .limitToLast(50)

        val options = FirebaseRecyclerOptions.Builder<FireMessage>()
            .setQuery(query, FireMessage::class.java)
            .setLifecycleOwner(this)
            .build()

        adapter = FireChatAdapter(application, options)
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                val messageCount = adapter.getItemCount()
                binding.messageList.scrollToPosition(messageCount - 1)
            }
        })

        binding.messageList.adapter = adapter
        username = FirebaseAuth.getInstance().currentUser?.displayName.toString()
        photourl = FirebaseAuth.getInstance().currentUser?.photoUrl.toString()
        uid = FirebaseAuth.getInstance().currentUser?.uid.toString()

        binding.sendButton.setOnClickListener {
            if (!binding.messageEditText.text.isEmpty()) {
                val message = FireMessage(
                    binding.messageEditText.text.toString(),
                    username!!, photourl, null,
                    datetime()
                )
                Log.i(
                    LOG_TAG, "sending: ${binding.messageEditText.text.toString()}" +
                            " username=${username}"
                )

                FirebaseDatabase.getInstance()
                    .reference
                    .child(MESSAGES_CHILD)
                    .push().setValue(message)

                binding.messageEditText.text.clear()
                hideSoftKeyboard((activity as AppCompatActivity))
            }
        }

        binding.addMessageImageView.setOnClickListener {
            // We don't have to create intent anymore if we're using contract GetContent
            // Just specify the mime type as a string
            // And we'll get Uri directly
            // https://developer.android.com/training/basics/intents/result
            openImageActivity.launch("image/*")
        }

        setHasOptionsMenu(true)
        update(FirebaseAuth.getInstance().currentUser != null)
        return binding.root
    }

    // General activity result contract
    private val openImageActivity =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            if (uri != null) {
                Log.i(LOG_TAG, "got result: " + uri.toString())
                val message = FireMessage(
                    null, username!!, photourl, LOADING_IMAGE_URL,
                    datetime()
                )
                val mFirebase = FirebaseDatabase.getInstance().reference.child(MESSAGES_CHILD)
                mFirebase.push().setValue(message) { error, ref ->
                    //setValue operation is done, you'll get null in errror and ref is the path reference for firebase database
                    val key = ref.key
                    Log.i(LOG_TAG, "got key: ${key} uri=${uri}")
                    val storageReference = key?.let { it1 ->
                        uri.lastPathSegment?.let { it2 ->
                            FirebaseStorage.getInstance()
                                .getReference(uid)
                                .child(it1)
                                .child(it2)
                        }
                    }
                    if (storageReference != null) {
                        putImageInStorage(storageReference, uri, key)
                    }
                }
            }
        }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeAuthenticationState()
    }

    private fun updateMenu(loggedIn: Boolean) {
        Log.i(LOG_TAG, "updateMenu: loggedIn=${loggedIn}")
        if (loggedIn) {
            optionsMenu?.findItem(R.id.login_menu)?.setVisible(false)
            optionsMenu?.findItem(R.id.logout_menu)?.setVisible(true)
            optionsMenu?.findItem(R.id.username_menu)?.setVisible(true)
            optionsMenu?.findItem(R.id.username_menu)?.title =
                FirebaseAuth.getInstance().currentUser?.displayName
        } else {
            optionsMenu?.findItem(R.id.login_menu)?.setVisible(true)
            optionsMenu?.findItem(R.id.logout_menu)?.setVisible(false)
            optionsMenu?.findItem(R.id.username_menu)?.setVisible(false)
        }
    }

    private fun update(loggedIn: Boolean) {
        with(binding) {
            if (loggedIn) {
                messageList.visibility = View.VISIBLE
                sendMessageLayout.visibility = View.VISIBLE
                authMessage.visibility = View.GONE
            } else {
                messageList.visibility = View.GONE
                sendMessageLayout.visibility = View.GONE
                authMessage.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
        optionsMenu = menu
        updateMenu(FirebaseAuth.getInstance().currentUser != null)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.login_menu -> {
                authResultLauncher.launch(SIGN_IN_REQUEST_CODE)
                return true
            }
            R.id.logout_menu -> {
                AuthUI.getInstance().signOut(requireContext())
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private val authResultLauncher =
        registerForActivityResult(AuthResultContract()) { idpResponse ->
            handleAuthResponse(idpResponse)
        }

    private fun handleAuthResponse(idpResponse: IdpResponse?) {
        // https://medium.com/@Marlonlom/adapting-firebaseauth-for-the-new-activity-result-api-d47b923cabc0
        when {
            (idpResponse == null || idpResponse.error != null) -> {
                /* Handle error from returned data. */
                Log.i(LOG_TAG, "Sign in unsuccessful ${idpResponse?.error?.errorCode}")
            }
            else -> {
                /* Handle sign-in success from returned data. */
                Log.i(
                    LOG_TAG,
                    "Successfully signed in user ${FirebaseAuth.getInstance().currentUser?.displayName}!"
                )
                username = FirebaseAuth.getInstance().currentUser?.displayName.toString()
                photourl = FirebaseAuth.getInstance().currentUser?.photoUrl.toString()
                uid = FirebaseAuth.getInstance().currentUser?.uid.toString()
            }
        }
    }

    /**
     * Observes the authentication state and changes the UI accordingly.
     * If there is a logged in user: (1) show a logout button and (2) display their name.
     * If there is no logged in user: show a login button
     */
    private fun observeAuthenticationState() {
        // Use the authenticationState variable from LoginViewModel to update the UI
        //  accordingly.
        //
        //  If there is a logged-in user, authButton should display Logout. If the
        //   user is logged in, you can customize the welcome message

        // If there is no logged in user, authButton should display Login and launch the sign
        //  in screen when clicked. There should also be no personalization of the message
        //  displayed.

        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                LoginViewModel.AuthenticationState.AUTHENTICATED -> {
                    updateMenu(true)
                    update(true)
                }
                else -> {
                    updateMenu(false)
                    update(false)
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if (activity.getCurrentFocus() == null) {
            return
        }
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()!!.getWindowToken(), 0)
    }

    private fun putImageInStorage(storageReference: StorageReference, uri: Uri, key: String) {
        storageReference.putFile(uri).addOnSuccessListener {
            it.metadata?.reference?.downloadUrl?.addOnCompleteListener {
                if (it.isSuccessful) {
                    var imgURL = it.result.toString()
                    Log.i(LOG_TAG, "uploaded image url: ${imgURL} key=${key}")
                    var message = FireMessage(null, username!!, photourl, imgURL, datetime())
                    FirebaseDatabase.getInstance()
                        .reference
                        .child(MESSAGES_CHILD).child(key)
                        .setValue(message);
                } else {
                    Log.i(LOG_TAG, "image upload failed: " + it.exception.toString())
                }
            }
        }
    }

    private fun datetime(): String {
        return Instant.now().atZone(ZoneId.systemDefault()).toLocalDateTime().format(
            DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm")
        )
    }

    class AuthResultContract : ActivityResultContract<Int, IdpResponse>() {
        // https://medium.com/@Marlonlom/adapting-firebaseauth-for-the-new-activity-result-api-d47b923cabc0
        companion object {
            // not sure what does it do - tells the launcher that it expects
            // an integer input parameter?
            const val INPUT_INT = "input_int"
        }

        private val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build()
        )

        override fun createIntent(context: Context, input: Int?): Intent = AuthUI.getInstance()
            .createSignInIntentBuilder()
            //.setLogo(R.drawable.img_auth_logo)
            //.setTheme(R.style.Theme_MyTheme_Auth)
            .setAvailableProviders(providers)
            .setIsSmartLockEnabled(false)  // https://stackoverflow.com/questions/52517730/firebase-ui-auth-sign-in-error-api-exception
            .build().apply { putExtra(INPUT_INT, input) }

        override fun parseResult(resultCode: Int, intent: Intent?): IdpResponse? =
            when (resultCode) {
                RESULT_OK -> IdpResponse.fromResultIntent(intent)
                else -> null
            }

    }

}

