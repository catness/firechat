package org.catness.firechat

data class FireMessage(
    var text: String? = "",
    var name: String = "",
    var photoUrl: String? = null,
    var imageUrl: String? = null,
    var datetime: String? = null,
    var id: String? = null
)

