package org.catness.firechat

import android.content.Context
import android.net.Uri
import android.provider.Settings.Global.getString
import android.text.Html
import android.text.Html.FROM_HTML_MODE_LEGACY
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import org.catness.firechat.FireChatAdapter.ChatViewholder
import org.catness.firechat.R.*

// FirebaseRecyclerAdapter is a class provided by 
// FirebaseUI. it provides functions to bind, adapt and show 
// database contents in a Recycler View 
class FireChatAdapter(
    val context: Context,
    options: FirebaseRecyclerOptions<FireMessage?>
) : FirebaseRecyclerAdapter<FireMessage, ChatViewholder>(options) {
    private val LOG_TAG: String = this.javaClass.simpleName

    override fun onBindViewHolder(
        holder: ChatViewholder,
        position: Int, model: FireMessage
    ) {
        val name = TextUtils.htmlEncode(model.name)
        val str = context.getString(string.username_datetime,name,model.datetime)
        // https://developer.android.com/guide/topics/resources/string-resource
        holder.name.text = Html.fromHtml(str,FROM_HTML_MODE_LEGACY)
        Log.i(
            LOG_TAG,
            "name=${model.name} text=${model.text} icon=${model.photoUrl} image=${model.imageUrl} datetime=${model.datetime}"
        )
        if (model.photoUrl != null && model.photoUrl != "null") {
            Glide.with(context)
                .load(model.photoUrl)
                .into(holder.icon);
        } else {
            holder.icon.setImageDrawable(
                ContextCompat.getDrawable(context, drawable.ic_account_circle_black_36dp)
            )
        }
        if (model.text != null && model.text != "null") {
            holder.text.text = model.text
            holder.text.visibility = View.VISIBLE
            holder.image.visibility = View.GONE
        }
        if (model.imageUrl != null && model.imageUrl != "null") {
            if (model.imageUrl!!.startsWith("gs://")) {
                var storageReference = FirebaseStorage.getInstance()
                    .getReferenceFromUrl(model.imageUrl!!)
                storageReference.downloadUrl.addOnSuccessListener {
                    Glide.with(context)
                        .load(it)
                        .into(holder.image);
                    holder.text.visibility = View.GONE
                    holder.image.visibility = View.VISIBLE
                }.addOnFailureListener {
                    Log.i(LOG_TAG, "can't load image ${model.imageUrl}: " + it.toString())
                    holder.text.text = "Can't load image ${model.imageUrl}"
                    holder.text.visibility = View.VISIBLE
                    holder.image.visibility = View.GONE
                }
            } else {
                Glide.with(context)
                    .load(model.imageUrl)
                    .into(holder.image)
                holder.text.visibility = View.GONE
                holder.image.visibility = View.VISIBLE

            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChatViewholder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layout.item_message, parent, false)
        return ChatViewholder(view)
    }

    class ChatViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView = itemView.findViewById(id.messageTextView)
        var name: TextView = itemView.findViewById(id.messengerTextView)
        var icon: ImageView = itemView.findViewById(id.messengerImageView)
        var image: ImageView = itemView.findViewById(id.messageImageView)
    }
}