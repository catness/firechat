# About the project

**Firechat** is an example for [Advanced Android in Kotlin 06.1-2: Firebase Login](https://developer.android.com/codelabs/advanced-android-kotlin-training-login), as well as [Firebase Android Codelab: Build Friendly Chat](https://codelabs.developers.google.com/codelabs/firebase-android). The chat codelab is in Java, so I converted it to Kotlin and changed the structure according to the Firebase Login codelab. Also added the date/time field.

In addition, Firechat uses the new Activity Results API instead of the deprecated startActivityForResult()/onActivityResult().
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
